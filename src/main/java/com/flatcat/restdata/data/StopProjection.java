package com.flatcat.restdata.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "stop", types = Stop.class)
public interface StopProjection {
    Long getId();
    String getDetails();

    @Value("#{T(com.flatcat.restdata.data.StopProjection).vehicleToRouteIdConverter(target.vehicle)}")
    String getRouteId();

    Route getRoute();

    static String vehicleToRouteIdConverter(Integer vehicle) {
        return String.valueOf((char)(vehicle + 'A' - 1));
    }
}
